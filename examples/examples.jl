using Revise
using StayOnTheRidge
using Symbolics

function example1()
    γ = 1e-3
    ϵ = 1e-2
    domain = Hyperrectangle([[-1,1] for _ in 1:2])
    dims = [1 for _ in 1:2]
    x = [Symbolics.variable(:x,i) for i in 1:2]
    u1 = 2*x[1]*x[2] + 3*x[2]^3 - 2*x[1]^3 - x[1] - 3*x[1]^2*x[2]^2
    u2 = 2*x[1]^2*x[2]^2 - 4*x[2]^3 - x[1]^2 + 4*x[2] + x[1]^2*x[2]
    return Config_sym([u1,u2], dims, x, γ, ϵ, domain)
end

config1 = example1();
elapsed1 = @elapsed fone1, trajectory1, m1, k1 = run_dynamics(config1)
pretty_print(fone1, elapsed1, m1, k1)
is_solution_VI(fone1, config1, config1.domain)
plot_trajectory2D(fone1, trajectory1, config1.domain)

function example2()
    γ = 1e-3
    ϵ = 1e-2
    domain = Hyperrectangle([[-1,1] for _ in 1:3])
    dims = [1 for _ in 1:3]
    x = [Symbolics.variable(:x,i) for i in 1:3]
    u1 = 1+2*x[1]+3*x[1]^2+2*x[2]*x[3]+4*x[1]*x[2]*x[3]+6*x[1]^2*x[2]*x[3]+3*x[2]^2*x[3]^2+6*x[1]*x[2]^2*x[3]^2+9*x[1]^2*x[2]^2*x[3]^2
    u2 = 7+2*x[1]+3*x[1]^2+2*x[2]+4*x[1]*x[2]+6*x[1]^2*x[2]+3*x[3]^2+6*x[1]*x[3]^2+9*x[1]^2*x[3]^2
    u3 = -x[3]-2*x[1]*x[3]-3*x[1]^2*x[3]-2*x[2]*x[3]-4*x[1]*x[2]*x[3]-6*x[1]^2*x[2]*x[3]-3*x[2]*x[3]^2-6*x[1]*x[2]*x[3]^2-9*x[1]^2*x[2]*x[3]^2
    return Config_sym([u1,u2,u3], dims, x, γ, ϵ, domain)
end

config2 = example2();
elapsed2 = @elapsed fone2, trajectory2, m2, k2 = run_dynamics(config2)
pretty_print(fone2, elapsed2, m2, k2)
is_solution_VI(fone2, config2, config2.domain)

function example3()
    γ = 1e-3
    ϵ = 1e-2
    domain = Hyperrectangle([[0.2,2] for _ in 1:6])
    dims = [1 for _ in 1:6]
    β = [0.5,0.51,0.52,0.3,0.31,0.32]
    a = [0.261, 0.494, 0.107, 0.366, 0.208, 0.305]
    n₀ = 0.43e-6
    ϕ = [
            7.463e-5 7.378e-5 7.293e-5 7.210e-5 7.127e-5 6.965e-5;
            7.451e-5 7.365e-5 7.281e-5 7.198e-5 7.115e-5 6.953e-5;
            7.438e-5 7.353e-5 7.269e-5 7.186e-5 7.103e-5 6.942e-5;
            7.427e-5 7.342e-5 7.258e-5 7.175e-5 7.093e-5 6.931e-5;
            7.409e-5 7.324e-5 7.240e-5 7.157e-5 7.075e-5 6.914e-5;
            7.387e-5 7.303e-5 7.219e-5 7.136e-5 7.055e-5 6.894e-5]
    x = [Symbolics.variable(:x,i) for i in 1:6]
    uis = Num[]
    for i in 1:6
        γi = x[i]/(n₀+sum(ϕ[i,:].*x))
        ui = -(x[i]-β[i]*(log(1+a[i]*(γi/(1-ϕ[i,i]*γi)))-x[i]))
        push!(uis, ui)
    end
    return Config_sym(uis, dims, x, γ, ϵ, domain)
end

config3 = example3();
elapsed3 = @elapsed fone3, trajectory3, m3, k3 = run_dynamics(config3)
pretty_print(fone3, elapsed3, m3, k3)
is_solution_VI(fone3, config3, config3.domain)

function example4()
    γ = 1e-3
    ϵ = 1e-2
    domain = Hyperrectangle([[-1,1] for _ in 1:2])
    dims = [1 for _ in 1:2]
    x = [Symbolics.variable(:x,i) for i in 1:2]
    u1 = 2*x[1]*x[2]^2-x[1]^2-x[2]
    return Config_sym([u1,-u1], dims, x, γ, ϵ, domain)
end

config4 = example4();
elapsed4 = @elapsed fone4, trajectory4, m4, k4 = run_dynamics(config4)
pretty_print(fone4, elapsed4, m4, k4)
is_solution_VI(fone4, config4, config4.domain)
plot_trajectory2D(fone4, trajectory4, config4.domain)

function example5()
    γ = 1e-3
    ϵ = 1e-2
    domain = Default()
    dims = [1 for _ in 1:3]
    x = [Symbolics.variable(:x,i) for i in 1:3]
    u1 = -2*x[1]*x[2]^2-2*x[1]^2+5*x[1]*x[2]-4*x[1]*x[3]-x[2]-2*x[3]
    u2 = 2*x[1]*x[2]^2-2*x[2]*x[3]^2-2*x[1]^2-5*x[1]*x[2]-2*x[2]^2+5*x[2]*x[3]+x[2]
    u3 = 2*x[2]*x[3]^2+4*x[1]^2+4*x[1]*x[3]+2*x[2]^2-5*x[2]*x[3]+2*x[3]
    return Config_sym([u1,u2,u3], dims, x, γ, ϵ, domain)
end

config5 = example5();
elapsed5 = @elapsed fone5, trajectory5, m5, k5 = run_dynamics(config5)
pretty_print(fone5, elapsed5, m5, k5)
is_solution_VI(fone5, config5, config5.domain)

function example6()
    γ = 1e-4
    ϵ = 1e-4
    domain = Default()
    dims = [2,1]
    x = [Symbolics.variable(:x,i) for i in 1:3]
    m = 1
    gamma = 0.2
    u1 = sum(x[i+1] * binomial(m,i) * x[3]^i*(1-x[3])^(m-i) for i in 0:1) - 2.0^(-m)*gamma*sum(x[i+1] * binomial(m,i) for i in 0:m) - 1
    u2 = -(x[3]-0.8)^2 + sum(binomial(m,i)*(1-x[i+1])*x[3]^i*(1-x[3])^(m-i) for i in 0:m)
    return Config_sym([u1,u2], dims, x, γ, ϵ, domain)
end

config6 = example6();
elapsed6 = @elapsed fone6, trajectory6, m6, k6 = run_dynamics(config6)
pretty_print(fone6, elapsed6, m6, k6)
is_solution_VI(fone5, config5, config5.domain)
