module StayOnTheRidge

include("algorithm.jl")
include("utils.jl")

export run_dynamics, Default, Hyperrectangle, Config_sym, plot_trajectory2D, pretty_print, format_elapsed, is_solution_VI

end


