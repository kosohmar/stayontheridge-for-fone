"""
Config_sym

A configuration structure to store data for STON'R execution, utilizing Symbolics.jl for differentiation.

# Arguments
- `uis::Vector{Num}` : Vector of symbolic utility functions.
- `dims::Vector{Int}` : Vector of dimensions for individual strategy sets.
- `x::Num` : Vector of symbolic variables.
- `γ::Real` : Step size.
- `ϵ::Real` : Exit point error.
- `domain::Domain` : Specifies the strategy sets (see Default or Hyperrectangle).

# Example
```julia-repl
julia> x = [Symbolics.variable(:x,i) for i in 1:2]
julia> u1 = 2*x[1]*x[2]^2-x[1]^2-x[2]
julia> uis = [u1,-u1]
julia> dims = [1,1]
julia> γ = 1e-2
julia> ϵ = 1e-2
julia> domain = Default()
julia> conf = Config_sym(uis, dims, x, γ, ϵ, domain);
```
"""
struct Config_sym
    v_tfmd::Vector{Function}
    v::Vector{Function}
    J::Matrix{Function}
    x::Vector{Num}
    d::Int
    γ::Real
    ϵ::Real
    domain::Domain

    function Config_sym(uis::Vector{Num}, dims::Vector{Int}, x::Vector{Num}, γ::Real, ϵ::Real, domain::Domain)
        d = sum(dims)
        @assert γ > 0
        @assert ϵ > 0
        @assert length(x) == d
        @assert length(dims) == length(uis)

        count = 1
        v_temp = Num[]
        for i in eachindex(uis)
            for _ in 1:dims[i]
                ∂ui_∂xi = Symbolics.derivative(uis[i],x[count])
                push!(v_temp, ∂ui_∂xi)
                count += 1
            end
        end

        v = [eval(build_function(v_temp[i], x)) for i in 1:d]

        if domain isa Default
            v_tfmd_temp = v_temp
        else
            v_tfmd_temp = prepare_v_tfmd_temp(uis, x, dims, domain)
        end

        v_tfmd = [eval(build_function(v_tfmd_temp[i], x)) for i in 1:d]
        J_expr = Symbolics.jacobian(v_tfmd_temp, x)
        J = [eval(build_function(J_expr[i,j], x)) for i in 1:d, j in 1:d]
        return new(v_tfmd, v, J, x, d, γ, ϵ, domain)
    end
end

eval_expr(expr, dict) = Symbolics.value.(Symbolics.substitute(expr, dict))

function prepare_v_tfmd_temp(uis, x, dims, domain)
    uis_tfmd = []
    dict = Dict(zip(x,H(domain)(x)))

    for ui in uis
        ui_tfmd = eval_expr(ui,dict)
        push!(uis_tfmd,ui_tfmd)
    end
    
    count = 1
    v_tfmd_temp = Num[]
    for i in eachindex(uis_tfmd)
        for _ in 1:dims[i]
            ∂ui_∂xi = Symbolics.derivative(uis_tfmd[i],x[count])
            push!(v_tfmd_temp, ∂ui_∂xi)
            count += 1
        end
    end
    return v_tfmd_temp
end
