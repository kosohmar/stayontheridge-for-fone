using LinearAlgebra
using Symbolics

include("domain.jl")
include("config.jl")

P(x; x_min = 0, x_max = 1) = min.(max.(x, x_min), x_max)

function get_v_tfmd(conf::Config_sym, point)
    v_tfmd = [conf.v_tfmd[i](point) for i in 1:conf.d]
    return v_tfmd
end

function get_v(conf::Config_sym, point)
    v = [conf.v[i](point) for i in 1:conf.d]
    return v
end

function get_vi(conf::Config_sym, point, i)
    vi = conf.v_tfmd[i](point)
    return vi
end

function get_v_tr(conf::Config_sym, new_point, tr_coords)
    v_tr = [conf.v_tfmd[tr](new_point) for tr in tr_coords]
    return v_tr
end

function get_jacobian_Si(conf::Config_sym, point, d_nonzero_idxs, S)
    jacobian_Si = [conf.J[i,j](point) for i in S, j in d_nonzero_idxs]
    return jacobian_Si
end

function compute_direction(point, i, S, conf::Config_sym)
    d = zeros(conf.d)

    if isempty(S)
        d[i] = 1
        return d
    end

    d_nonzero_idxs = vcat(S, [i])
    jacobian_Si = get_jacobian_Si(conf, point, d_nonzero_idxs, S)
    d_nonzero = nullspace(jacobian_Si)
    if size(d_nonzero, 2) != 1
        # direction is not uniquely defined
        error("Assumption 1 violated at i = $i, S = $S, x = $point, nullspace dimension is $(ndims(d_nonzero)), jacobian_Si_at_point = $jacobian_Si")
    end

    d_nonzero = vec(d_nonzero)
    decision_mat = hcat(transpose(jacobian_Si), d_nonzero)
    decision_det = det(decision_mat)
    if sign(decision_det) != (-1)^(length(S))
        d_nonzero .*= -1
    end

    d[d_nonzero_idxs] = d_nonzero
    return d
end

function good_exit(point, i, conf::Config_sym)
    vi = get_vi(conf, point, i)
    xi = point[i]
    zs = abs(vi) <= conf.ϵ
    return zs || (xi == 0 && vi < conf.ϵ) || (xi == 1 && vi > -conf.ϵ), zs
end

function bad_exit(point, i, S, direction)
    tr_coords = vcat(S, [i])
    point_tr = point[tr_coords]
    direction_tr = direction[tr_coords]

    for (j, (pt, dt)) in enumerate(zip(point_tr, direction_tr))
        if (pt == 0 && dt < 0) || (pt == 1 && dt > 0)
            return tr_coords[j]
        end
    end

    return 0
end

function middling_exit(point, i, S, direction, conf::Config_sym)
    new_point = point + conf.γ*direction
    tr_coords = setdiff(1:i-1, S)
    point_tr = point[tr_coords]
    v_tr = get_v_tr(conf, new_point, tr_coords)
    for (j, (pt, vt)) in enumerate(zip(point_tr, v_tr))
        if (pt == 0 && vt > 0) || (pt == 1 && vt < 0)
            return tr_coords[j]
        end
    end

    return 0
end

function vx_wrst_case_y(dom::Hyperrectangle, conf::Config_sym, point)
    vx = get_v(conf, point)
    y_worst = []
    for i in eachindex(vx)
        if vx[i] < 0
            push!(y_worst, dom.sides[i][1])
        else
            push!(y_worst, dom.sides[i][2])
        end
    end
    return vx, y_worst
end

function vx_wrst_case_y(dom::Default, conf::Config_sym, point)
    vx = get_v(conf, point)
    return vx, ifelse.(vx .<= 0, 0, 1)
end

# check if point satisfies the variational inequality
function is_solution_VI(point, conf::Config_sym; α = 0.1)
    vx = get_v_tfmd(conf, point)
    y_worst = ifelse.(vx .<= 0, 0, 1)
    dot1 = dot(vx, point)
    dot2 = dot(vx, y_worst)
    VI_val = dot1 - dot2
    return VI_val >= -α
end

function is_solution_VI(point, conf::Config_sym, dom::Domain; α = 0.1)
    vx, y_worst = vx_wrst_case_y(dom, conf, point)
    dot1 = dot(vx, point)
    dot2 = dot(vx, y_worst)
    VI_val = dot1 - dot2
    if VI_val >= -α
        display("$α-VI satisfied at $point with value $VI_val")
    else
        @warn "$α-VI not satisfied at $point with value $VI_val"
    end
    return VI_val >= -α
end

"""
run_dynamics(conf::Config_sym)

Executes STON'R dynamics with a given configuration conf.

# Arguments
- `conf::Config_sym` : See `Config_sym`.

# Output
- `point`: The FONE.
- `pts`: A vector of trajectory points explored during the dynamics.
- `m`: The number of epochs (outer iterations) performed.
- `k`: The number of iterations within each epoch.

# Example

```julia-repl
julia> γ = 1e-3
julia> ϵ = 1e-2
julia> domain = Hyperrectangle([[-1,1],[-1,1]])
julia> dims = [1,1]
julia> x = [Symbolics.variable(:x,i) for i in 1:2]
julia> u1 = 2*x[1]*x[2]^2-x[1]^2-x[2]
julia> conf = Config_sym([u1,-u1], dims, x, γ, ϵ, domain);
julia> fone, trajectory, m, k = run_dynamics(conf)
```
"""
function run_dynamics(conf::Config_sym)
    point = fill(0, conf.d)
    i = 1
    S = Vector{Int}()
    pts = Vector{Vector{Float64}}()
    m = 0
    k = 0

    while  i <= conf.d
        k = 0
        println("starting epoch ($i, $S) at $point")

        while true
            is_good_exit, is_zs = good_exit(point, i, conf)
            if is_good_exit
                println("good exit at $point")
                if is_zs
                    S = vcat(S,[i])
                end
                i += 1
                break
            end
            
            direction = compute_direction(point, i, S, conf)
            j = bad_exit(point, i, S, direction)
            if j != 0
                println("bad exit at $point for j = $j")
                if j == i
                    S = setdiff(S, [i-1])
                    i -= 1
                else
                    S = setdiff(S, [j])
                end
                break
            end

            j = middling_exit(point, i, S, direction, conf)
            if j != 0
                println("middling exit at $point for j = $j")
                S = vcat(S, [j])
                break
            end
            
            point += conf.γ*direction
            point = P(point)

            k += 1
            push!(pts, point)
        end

        m += 1
    end
    return conf.domain isa Default ? (point, pts, m, k) :
        (H(conf.domain)(point), map(x->H(conf.domain)(x),pts), m, k)
end